import React from 'react';
import { Admin, Resource } from 'react-admin';

import { myAuthProvider } from './authProvider/authProvider'
import LoginPage from './authProvider/loginPage'
import logoutButton from './authProvider/logoutButton'
import { drawGantt,monika, jobCreate, jobDataGrid, jobShow, p5js } from './dataProvider/job';
import { resourceDataGrid, resourceEdit } from './dataProvider/resource';
import { myDataProvider } from './dataProvider/dataProvider'

import * as ra from '../types/react-admin';

import resourceIcon from '@material-ui/icons/Memory';
import jobIcon from '@material-ui/icons/Work';

const dataProvider : ra.DataProvider = myDataProvider;
const authProvider: ra.AuthProvider = myAuthProvider;

const App = () => (
    // <iframe src="http://gist-it.appspot.com/https://github.com/oar-team/oar3/blob/master/visualization_interfaces/DrawGantt-SVG/test.svg" title='test' height="700px" width="50%" ></iframe> 

    <Admin dataProvider={dataProvider} authProvider={authProvider} logoutButton={logoutButton} loginPage={LoginPage} >
        <Resource name="jobs" list={jobDataGrid} create={jobCreate} show={jobShow} icon={jobIcon}/>
        <Resource name="resources" list={resourceDataGrid} edit={resourceEdit} icon={resourceIcon} />
        <Resource name="Drawgantt" list={drawGantt} />
        <Resource name="Monika" list={monika} />
        <Resource name="P5js"  list={p5js} />
    </Admin>
);

export default App;
