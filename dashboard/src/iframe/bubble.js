// Bubble class
class Bubble {
  constructor(x, y, diameter, name, resList,squaresObject) {
    this.x = x;
    this.y = y;
    this.diameter = diameter;
    this.radius = diameter / 2;
    this.name = name;
    this.resList = resList;
    this.over = false;
    this.squaresObject = squaresObject;
  }
  // Check if mouse is over the bubble
  rollover(px, py) {
    let d = dist(px, py, this.x, this.y);
    this.over = d < this.radius;
  }
  // Display the Bubble
  display() {
    stroke(0);
    strokeWeight(1);
    ellipse(this.x, this.y, this.diameter, this.diameter);
    fill(200);
    textAlign(CENTER);
    textSize(this.diameter / 4)
    text(this.name, this.x, this.y + this.radius + this.radius / 2);
    if (this.over) {
      for (const square in this.squaresObject) {
        this.squaresObject[square].actionOver();
      }
      cursor(HAND);
      let aliveRes = "";
      let downRes = "";
      let alone = 0;
      for (const resource in this.resList) {
        if (resource != undefined) {
          if (this.resList[resource].state == "Alive") {
            aliveRes += resource + ",";
            alone += 1;
          }
          else {
            downRes += resource + ",";
            alone += 1;
          }
          let textSz = this.diameter / 9;
          textSize(textSz);
          textLeading(textSz + 3);
          textStyle(BOLD);
          if (aliveRes != "") {
            fill(10, 200, 10);
            text("Resource alive :\n " + aliveRes.slice(0, -1), this.x, this.y - alone * textSz / 2);
          }
          if (downRes != "") {
            fill(200, 10, 10);
            text("Resource down :\n" + downRes.slice(0, -1), this.x, this.y + alone * (textSz * 1.4) / 2);
          }
        }
      }
      textStyle(NORMAL);
      fill(200);
    }
  }
}
