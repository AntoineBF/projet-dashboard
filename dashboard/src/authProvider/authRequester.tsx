import { fetchUtils } from 'react-admin';

const httpClient = fetchUtils.fetchJson;

//Retrieve the current user ids and making an authenticated request

export const AuthRequest = (url: string, params?: any) => {

	let resp: any;
	//const notify = useNotify();

	if (localStorage.getItem('is_connected') === 'true') {

		let username: string | null = localStorage.getItem('username');
		let password: string | null = localStorage.getItem('password');

		let headers: Headers = new Headers({
			'Authorization': 'Basic ' + btoa(username + ':' + password),
			'Content-Type': 'application/json',
			'Accept': "application/json"
		});

		params.headers = headers;

		resp = httpClient(url, params).catch(function () {
			throw new Error("You can't perform this action as "+username); //Improvement to do : should be notify
			//notify("You can't perform this action as"+username,'warning');
		});
	}
	else {
		//Storing the aborted request to do it once connected
		localStorage.setItem('abortedRequestUrl', url);
		localStorage.setItem('abortedRequestParams', JSON.stringify(params));

		resp = loginRedirect();
	}

	return resp;
}

export function loginRedirect(): any {
	localStorage.setItem('redirect_to_login', 'true');
	return httpClient('/oarapi/authentication'); //Trigger redirection
}