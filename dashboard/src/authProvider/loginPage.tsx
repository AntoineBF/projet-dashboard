import React, {
	HtmlHTMLAttributes,
	ReactNode,
	useRef,
	useEffect,
	useMemo,
} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import {
	Card,
	Avatar,
	createMuiTheme,
	makeStyles,
	Theme,
} from '@material-ui/core';
import { ThemeProvider } from '@material-ui/styles';
import LockIcon from '@material-ui/icons/Lock';
import { StaticContext } from 'react-router';
import { useHistory } from 'react-router-dom';
import { useCheckAuth } from 'ra-core';

import defaultTheme from 'ra-ui-materialui/src/defaultTheme';
import DefaultLoginForm from './loginForm';

interface Props {
	backgroundImage?: string;
	children: ReactNode;
	classes?: object;
	className?: string;
	staticContext?: StaticContext;
	theme: object;
}

const useStyles = makeStyles(
	(theme: Theme) => ({
		main: {
			display: 'flex',
			flexDirection: 'column',
			minHeight: '100vh',
			height: '1px',
			alignItems: 'center',
			justifyContent: 'flex-start',
			backgroundRepeat: 'no-repeat',
			backgroundSize: 'cover',
			backgroundImage:
				'radial-gradient(circle at 50% 14em, #313264 0%, #00023b 60%, #00023b 100%)',
		},
		card: {
			minWidth: 300,
			marginTop: '6em',
		},
		avatar: {
			margin: '1em',
			display: 'flex',
			justifyContent: 'center',
		},
		icon: {
			backgroundColor: theme.palette.secondary[500],
		},
	}),
	{ name: 'RaLogin' }
);

const LoginPage: React.FunctionComponent<
	Props & HtmlHTMLAttributes<HTMLDivElement>
> = ({
	theme,
	classes: classesOverride,
	className,
	children,
	staticContext,
	backgroundImage,
	...rest
}) => {

		const containerRef = useRef<HTMLDivElement>(null);
		const classes = useStyles({ classes: classesOverride });
		const muiTheme = useMemo(() => createMuiTheme(theme), [theme]);
		let backgroundImageLoaded = false;
		const checkAuth = useCheckAuth();
		const history = useHistory();
		useEffect(() => {
			checkAuth({}, false)
				.then(() => {
					// already authenticated, redirect to the home page
					history.push('/');
				})
				.catch(() => {
					// not authenticated, stay on the login page
					localStorage.removeItem('redirect_to_login'); //The redirection to login has been successful
				});
		}, [checkAuth, history]);

		const updateBackgroundImage = () => {
			if (!backgroundImageLoaded && containerRef.current) {
				containerRef.current.style.backgroundImage = `url(${backgroundImage})`;
				backgroundImageLoaded = true;
			}
		};

		// Load background image asynchronously to speed up time to interactive
		const lazyLoadBackgroundImage = () => {
			if (backgroundImage) {
				const img = new Image();
				img.onload = updateBackgroundImage;
				img.src = backgroundImage;
			}
		};

		useEffect(() => {
			if (!backgroundImageLoaded) {
				lazyLoadBackgroundImage();
			}
		});

		return (
			<ThemeProvider theme={muiTheme}>
				<div
					className={classnames(classes.main, className)}
					{...rest}
					ref={containerRef}
				>
					<Card className={classes.card}>
						<div className={classes.avatar}>
							<Avatar className={classes.icon}>
								<LockIcon />
							</Avatar>
						</div>
						{children}
					</Card>
				</div>
			</ThemeProvider>
		);
	};

LoginPage.propTypes = {
	backgroundImage: PropTypes.string,
	children: PropTypes.node,
	classes: PropTypes.object,
	className: PropTypes.string,
	staticContext: PropTypes.object,
};

LoginPage.defaultProps = {
	theme: defaultTheme,
	children: <DefaultLoginForm />,
};

export default LoginPage;