import React, { forwardRef } from 'react';
import { useLogout } from 'react-admin';
import MenuItem from '@material-ui/core/MenuItem';
import ExitIcon from '@material-ui/icons/PowerSettingsNew';
import PersonIcon from '@material-ui/icons/Person';
import { useHistory } from 'react-router-dom';

const logoutButton: React.ForwardRefExoticComponent<React.RefAttributes<unknown>>
	= forwardRef((props, ref) => {
		const logout = useLogout();
		const handleClick = () => logout();
		const history = useHistory();

		const routeToLogin = () => {
			localStorage.setItem('redirect_to_login', 'true');
			let path = '/login';
			history.push(path);
		}

		if (localStorage.getItem('is_connected') === 'true') {
			return (
				<MenuItem
					onClick={handleClick}
				>
					<ExitIcon /> Logout
					<PersonIcon /> Connected as: {localStorage.getItem('username')}
				</MenuItem>
			);
		}
		else {
			return (
				<MenuItem
					onClick={routeToLogin}
				>
					<PersonIcon /> Login
				</MenuItem>
			);
		}
	});

export default logoutButton;